<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register',[
            "title"=>"Registration"
        ]);
    }
     public function kirim( Request $request){
         $namaDepan = $request['fname'];
         $namaBelakang = $request['lname'];
         return view('welcome',[
             'namaDepan'=>$namaDepan,
             "namaBelakang" => $namaBelakang
         ]);
     }
}


?>