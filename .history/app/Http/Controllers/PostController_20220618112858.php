<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
   public function create(){
        return view('post.create',["title"=>"Post"]);
   } 
   public function create(Request $request){
      $request->validate([
         'title' => 'required|unique:post',
         'body' => 'required',
      ]);
      $query = DB::table('posts')->insert([
         "title"=>$request["title"],
         "body"=>$request["body"],
      ]);
      return redirect('/posts');
   }
}
