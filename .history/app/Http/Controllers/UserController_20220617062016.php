<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        return view('home',[
            "title"=>"Home"
        ]);
    }
    public function register(){
        return view('register',[
            "title"=>"Registration"
        ]);
    }
     public function table(){
         return view('table',[
             "title"=>"Welcome"
         ]);
     }
    // public function kirim( Request $request){
    //     $namaDepan = $request['fname'];
    //     $namaBelakang = $request['lname'];
    //     return view('welcome',[
    //         'namaDepan'=>$namaDepan,
    //         "namaBelakang" => $namaBelakang
    //     ]);
    // }
}
