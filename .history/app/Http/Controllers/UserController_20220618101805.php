<?php

namespace App\Http\Controllers;



class UserController extends Controller
{
    public function index(){
        return view('home',[
            "title"=>"Home"
        ]);
    }
    
     public function table(){
         return view('table',[
             "title"=>"Welcome"
         ]);
     }
    
    public function datatables(){
        return view('datatables',["title"=>"Datatables"]);
    }
}
