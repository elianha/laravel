<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class,'index']);
Route::get('/register',[AuthController::class,'register']);
//Route::get('/welcome', [AuthController::class,'kirim']);
Route::get('/table',[UserController::class,'table']);
Route::get('/master',function(){
    return view('layouts.master');
});
Route::get('/data-tables',[UserController::class,'datatables']);
Route::get('post',[KategoriController::class,'index']);