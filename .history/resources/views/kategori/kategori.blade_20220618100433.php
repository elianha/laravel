@extends('layouts.master')

@section('title')
    Halaman Tambah Kategori
@endsection

@section('content')
<form>
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="email" name="nama" class="form-control">
  </div>
  <div class="form-group">
    <label>Deskripsi Kategori</label>
    <textarea type="password" class="form-control" name="deskripsi"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection