@extends('layouts.master')

@section('title')
    Halaman Tambah Kategori
@endsection

@section('content')
<form>
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label>Deskripsi Kategori</label>
    <input type="password" class="form-control" id="exampleInputPassword1">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection