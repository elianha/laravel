@extends('layouts.master')

@section('title')
    Halaman Tambah Kategori
@endsection

@section('content')
<form action="/posts" method="POST">
  @csrf
  <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">body</label>
                <input type="text" class="form-control" name="body" id="body" placeholder="Masukkan Body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <input type="submit" value="kirim">
</form>
@endsection