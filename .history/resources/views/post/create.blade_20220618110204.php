@extends('layouts.master')

@section('title')
    Halaman Tambah Kategori
@endsection

@section('content')
<form>
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="email" name="title" class="form-control">
  </div>
  <div class="form-group">
    <label>Deskripsi Kategori</label>
    <textarea type="password" class="form-control" name="body" rows="10" cols="30"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection