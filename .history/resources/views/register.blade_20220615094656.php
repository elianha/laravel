<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p>First Name :</label>
        <input type="text" placeholder="First Name" name="fname">

        <br><br>

        <p>Last Name :</label>
        <input type="text"placeholder="Last Name" name="lname">

        <br><br>
        <p>Gender :</label><br>
        <input type="radio" id="male" name="jeniskelamin">
        <label>Male</label><br>
        <input type="radio" id="female" name="jeniskelamin">
        <label>Female</label><br>
        <input type="radio" id="other" name="jeniskelamin">
        <label>Other</label><br><br>

        <p>Nationally</label><br>
        <select type="radio" name="nationally">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
        </select>
         <br> <br>
        <p>Language Spoken : </label><br>
        <input type="checkbox" id="lang1" name="lang1" value="Bahasa Indonesia">
        <label for="vehicle1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang2" name="lang2" value="English">
        <label for="vehicle2"> English </label><br>
        <input type="checkbox" id="lang3" name="lang3" value="Other">
        <label for="vehicle3"> Other </label><br><br>

        <p>Bio :</label><br>
        <textarea rows="10" cols="50"></textarea><br>
        <input type="submit" value="kirim">
    </form>  
</body>
</html>